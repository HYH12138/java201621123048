import java.util.Scanner;

class ArrayUtils{
	public static double findMax(double[] arr,int begin, int end)throws IllegalArgumentException{
		if(begin>=end){
			IllegalArgumentException e = new IllegalArgumentException("begin:"+begin+" >= end:"+end);
			throw e;
		}
		if(begin<0){
			IllegalArgumentException e = new IllegalArgumentException("begin:"+begin+" < 0");
			throw e;
		}
		if(end>arr.length){
			IllegalArgumentException e = new IllegalArgumentException("end:"+end+" > arr.length");
			throw e;
		}
		double Max = arr[begin] ;
		for(int i=begin;i<end;i++){
			if(arr[i]>Max)
				Max=arr[i];
		}
		return Max;
	}
}

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner (System.in); 
		int n = in.nextInt();
		double a[] = new double[n];
		for(int i=0;i<n;i++){
			a[i]=in.nextInt();
		}
		while(in.hasNextInt()){
			try{
				int begin = in.nextInt();
				int end = in.nextInt();
				System.out.println(ArrayUtils.findMax(a,begin,end));
			}
			catch(IllegalArgumentException e){
				System.out.println(e);
			}
			
		}
		try {
			     System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class,int.class,int.class));
			} catch (Exception e1) {
			}
	}

}
